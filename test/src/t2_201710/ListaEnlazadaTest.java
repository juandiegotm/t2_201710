package t2_201710;

import java.util.Iterator;

import junit.framework.TestCase;
import model.data_structures.ListaEncadenada;

public class ListaEnlazadaTest extends TestCase {

	private ListaEncadenada<String> listaEncadenada;

	public void setupEscenario1(){

		listaEncadenada = new ListaEncadenada<>();
		listaEncadenada.agregarElementoFinal("Soy la primera frase");
		listaEncadenada.agregarElementoFinal("+10 por ver este String");
		listaEncadenada.agregarElementoFinal("Vamos por un 5 en estructuras de datos");
	}

	public void setupEscenario2(){
		listaEncadenada = new ListaEncadenada<>();
		listaEncadenada.agregarElementoFinal("Soy el #1");
	}

	public void setupEscenario3(){
		listaEncadenada = new ListaEncadenada<>();
	}

	public void testPosicionesCorrectas()
	{
		setupEscenario1();
		assertEquals("El primer elemento de la lista no corresponde","Soy la primera frase", listaEncadenada.darElemento(0));
		assertEquals("El segundo elemento de la lista no corresponde","+10 por ver este String", listaEncadenada.darElemento(1));
		assertEquals("El tercer elemento de la lista no corresponde","Vamos por un 5 en estructuras de datos", listaEncadenada.darElemento(2));

		setupEscenario3();
		try{
			listaEncadenada.darElementoPosicionActual();
			fail();
		}

		catch(Exception e){

		}
	}

	public void testDesplazamientos(){

		setupEscenario1();
		assertEquals("No deber�a ser posible desplazarse a la posici�n anterior",false, listaEncadenada.retrocederPosicionAnterior());

		assertEquals("Deber�a ser posible desplazarse a la siguiente posici�n",true, listaEncadenada.avanzarSiguientePosicion());
		assertEquals("+10 por ver este String", listaEncadenada.darElementoPosicionActual());

		assertEquals("Deber�a ser posible desplazarse a la siguiente posici�n",true, listaEncadenada.avanzarSiguientePosicion());
		assertEquals("Vamos por un 5 en estructuras de datos", listaEncadenada.darElementoPosicionActual());

		assertEquals("No deber�a ser posible desplazarse a la siguiente posici�n",false, listaEncadenada.avanzarSiguientePosicion());
	}

	public void testTama�os(){
		setupEscenario1();
		assertEquals("El tama�o deberia ser de 3 elementos", 3, listaEncadenada.darNumeroElementos());

		setupEscenario2();
		assertEquals("El tama�o deberia ser de 1 elemento", 1, listaEncadenada.darNumeroElementos());

		setupEscenario3();
		assertEquals("El tama�o deberia ser de 1 elemento", 0, listaEncadenada.darNumeroElementos());
	}

	public void testeAgregarElementos(){
		setupEscenario3();
		listaEncadenada.agregarElementoFinal("Nuevo String");
		listaEncadenada.agregarElementoFinal("Nuevo String2");
		listaEncadenada.agregarElementoFinal("Nuevo String3");
		assertEquals("El ultimo elemento agregado coincide", "Nuevo String3", listaEncadenada.darElemento(2));

	}
	
	public void testIterator(){
		setupEscenario1();
		Iterator<String> it = listaEncadenada.iterator();
		assertEquals("S� hay siguiente elemento", true, it.hasNext());
		assertEquals("El primer elemento no coincide" ,"Soy la primera frase", it.next());
		
		assertEquals("S� hay siguiente elemento", true, it.hasNext());
		assertEquals("El segundo elemento no coincide" ,"+10 por ver este String", it.next());
		
		assertEquals("S� hay siguiente elemento", true, it.hasNext());
		assertEquals("El tercer elemento no coincide", "Vamos por un 5 en estructuras de datos", it.next());
		
		assertEquals("No hay siguiente elemento", false, it.hasNext());
		try{
			it.next();
			fail();
		}
		
		catch(Exception e){
			
		}
	}

}
