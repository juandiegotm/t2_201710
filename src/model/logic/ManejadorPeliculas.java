package model.logic;

import java.awt.List;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;

import model.data_structures.ILista;
import model.data_structures.ListaDobleEncadenada;
import model.data_structures.ListaEncadenada;
import model.vo.VOAgnoPelicula;
import model.vo.VOPelicula;
import api.IManejadorPeliculas;

public class ManejadorPeliculas implements IManejadorPeliculas {

	private ILista<VOPelicula> misPeliculas;

	private ILista<VOAgnoPelicula> peliculasAgno;


	@Override
	public void cargarArchivoPeliculas(String archivoPeliculas) {
		// TODO Auto-generated method stub
		try{
			File archivoCSV = new File(archivoPeliculas);
			BufferedReader lector = new BufferedReader(new FileReader(archivoCSV));
			misPeliculas = new ListaEncadenada<VOPelicula>();
			peliculasAgno = new ListaDobleEncadenada<VOAgnoPelicula>();

			String linea = lector.readLine();
			linea = lector.readLine();

			while(linea != null){
				// Recosntrucci�n del titulo de la pelicula

				String [] info = linea.split(",");
				String auxTitulo = "";


				for(int i = 1; i < info.length -1; i++)
					auxTitulo+= info[i] + ",";

				auxTitulo = auxTitulo.substring(0, auxTitulo.length()-1);
				if(auxTitulo.charAt(0) == '"')
					auxTitulo = auxTitulo.substring(1, auxTitulo.length() - 1);

				auxTitulo = auxTitulo.trim();


				// Obtenci�n fecha de la pelicula	
				String fechaEnCadena = "";
				int fecha = 0;
				boolean tieneFecha = false;

				if( auxTitulo.charAt(auxTitulo.length()-1) == ')'){

					tieneFecha = true;
					fechaEnCadena = auxTitulo.substring(auxTitulo.length()-5, auxTitulo.length()-1 );
					for(int i = 0; i< fechaEnCadena.length() && tieneFecha; i++){
						Character caracterActual = fechaEnCadena.charAt(i);
						if(!Character.isDigit(caracterActual))
							tieneFecha = false;
					}
				}

				//Si la pelicula tiene fecha, se le quita del titulo

				String nombre = auxTitulo;
				if(tieneFecha){
					nombre = auxTitulo.substring(0,auxTitulo.length()-6);
					fecha = Integer.parseInt(fechaEnCadena);
				}

				//Obtenci�n de los generos
				String aux[] = info[info.length-1].split("|");

				ListaEncadenada<String> generos = new ListaEncadenada<String>(); 
				for(int i = 0; i < aux.length; i++)
					generos.agregarElementoFinal(aux[i]);


				//Creaci�n de la pelicula y asignaci�n de sus atributos
				VOPelicula peliculaAAgregar = new VOPelicula();
				peliculaAAgregar.setAgnoPublicacion(fecha);
				peliculaAAgregar.setTitulo(nombre);
				peliculaAAgregar.setGenerosAsociados(generos);

				//A�adiendo las peliculas a la lista en desorden
				misPeliculas.agregarElementoFinal(peliculaAAgregar);

				linea = lector.readLine();
			}

		}
		catch(Exception e){

		}
		
		

		//Pasar MisPeliculas a arreglo para ordenar
		int i = 0;
		VOPelicula[] auxPelis = new VOPelicula[misPeliculas.darNumeroElementos()];
		for(VOPelicula peliculas: misPeliculas){
			auxPelis[i] = peliculas;
			i++;
		}
		ComparadorPeliculasPorAgno comparador = new ComparadorPeliculasPorAgno();
		Arrays.sort(auxPelis, comparador);

		//Crear y agregar la lista sin fechas
		VOAgnoPelicula listaSinFechas = new VOAgnoPelicula();
		listaSinFechas.setAgno(0);
		peliculasAgno.agregarElementoFinal(listaSinFechas);

		//Leer arreglo ordenado y crear las listas de peliculas
		ListaEncadenada<VOPelicula> listaActual = new ListaEncadenada<>();
		for(int k = 0, ultimoAgnoAgregado = 0, indListasPelicula = 0; k < auxPelis.length; k++){
			VOPelicula peliculaActual = auxPelis[k];
			
			if(ultimoAgnoAgregado == peliculaActual.getAgnoPublicacion()){
				listaActual.agregarElementoFinal(peliculaActual);
			}

			else{
				//1. A�adir las peliculas a la lista del ultimo a�o creado
				peliculasAgno.darElemento(indListasPelicula++).setPeliculas(listaActual);
				//2. Crear una nueva lista para el a�o de la pelicula actual
				VOAgnoPelicula nuevaListaPorAgno = new VOAgnoPelicula();
				nuevaListaPorAgno.setAgno(peliculaActual.getAgnoPublicacion());
				//3. Agregar la nueva lista a lista de las peliculas por Agno
				peliculasAgno.agregarElementoFinal(nuevaListaPorAgno);
				//4. Cambiar variable que indica si sigue agregando peliculas a una
				//misma lista o crea una nueva
				ultimoAgnoAgregado = peliculaActual.getAgnoPublicacion();
				//5. Volver a inicializar una nueva lista de peliculas y asiganar
				//la primera pelicula que dio origen a la lista nueva
				listaActual = new ListaEncadenada<>();
				listaActual.agregarElementoFinal(peliculaActual);
				

			}
		}
		
		//Agregar ultima lista a peliculasAgnio
		peliculasAgno.darElemento(peliculasAgno.darNumeroElementos()-1).setPeliculas(listaActual);
	}



	public ILista<VOPelicula> darListaPeliculas(String busqueda) {

		ListaEncadenada<VOPelicula> listaDeBusqueda = new ListaEncadenada<>();
		Iterator<VOPelicula> it = misPeliculas.iterator();
		while(it.hasNext()){

			VOPelicula peliculaActual = it.next();
			boolean corresponde = false;
			for(int i = 0, k = 0; i < peliculaActual.getTitulo().length() && !corresponde; i++){
				Character caracterActual = peliculaActual.getTitulo().charAt(i);
				Character caracterBusqueda = busqueda.charAt(k);

				if(caracterActual == caracterBusqueda){
					k++;
				}

				else if(busqueda.charAt(0) == caracterActual){
					k=0;
					i--;
				}


				else{
					k = 0;
				}

				if(k == busqueda.length())
					corresponde = true;

			}

			if(corresponde)
				listaDeBusqueda.agregarElementoFinal(peliculaActual);

		}

		return  listaDeBusqueda;

	}

	@Override
	public ILista<VOPelicula> darPeliculasAgno(int agno) {
		ListaEncadenada<VOPelicula> peliculasPorAgno = new ListaEncadenada<>();
		boolean encontrado = false;

		Iterator it = peliculasAgno.iterator();

		while(it.hasNext() && !encontrado){
			VOAgnoPelicula listaActual = (VOAgnoPelicula)it.next();
			if(listaActual.getAgno() == agno){
				peliculasPorAgno = (ListaEncadenada<VOPelicula>)listaActual.getPeliculas();
				encontrado = true;
			}
		}

		return peliculasPorAgno;
	}

	@Override
	/**
	 * Retorna el objeto con las peliculas del a�o siguiente. pero en caso de ser el ultimo, retorna el mismo objeto sin avanzar.
	 */
	public VOAgnoPelicula darPeliculasAgnoSiguiente() {
		// TODO Auto-generated method stub
		peliculasAgno.avanzarSiguientePosicion();
		return peliculasAgno.darElementoPosicionActual();
	}

	@Override
	/**
	 * Retorna el objeto con las peliculas del a�o anterior. pero en caso de ser la primera, retorna el mismo objeto sin retornar.
	 */
	public VOAgnoPelicula darPeliculasAgnoAnterior() {
		// TODO Auto-generated method stub
		peliculasAgno.retrocederPosicionAnterior();
		return peliculasAgno.darElementoPosicionActual();
	}

	public class ComparadorPeliculasPorAgno implements Comparator<VOPelicula> {

		@Override
		public int compare(VOPelicula pelicula1, VOPelicula pelicula2) {
			if(pelicula1.getAgnoPublicacion() > pelicula2.getAgnoPublicacion())
				return 1;

			else if(pelicula1.getAgnoPublicacion() < pelicula2.getAgnoPublicacion())
				return -1;

			else
				return 0;
		}




	}

}