package model.data_structures;

import java.util.Iterator;

public class ListaDobleEncadenada<T> implements ILista<T> {

	private DoubleNode<T> primero;
	private DoubleNode<T> nodoActual;
	private DoubleNode<T> ultimo;


	@Override
	public Iterator<T> iterator() {
		MiIterador<T> it = new MiIterador<T>(primero);
		return it;
	}	

	@Override
	public void agregarElementoFinal(T elem) {
		// TODO Auto-generated method stub
		DoubleNode<T> nodoAgregar = new DoubleNode<T>(elem);
		if(primero == null){
			primero = nodoAgregar;
			nodoActual = primero;
			ultimo = primero;
		}

		else{
			
			ultimo.next = nodoAgregar;
			nodoAgregar.previous = ultimo;
			ultimo = nodoAgregar;

		}
	}

	@Override
	public T darElemento(int pos) {
		// TODO Auto-generated method stub
		DoubleNode<T> actual = primero;
		while(actual != null && pos > 0)
		{
			actual = actual.next;
			pos--;
		}
		if(pos > 0)
			throw new IndexOutOfBoundsException();
		else
			return actual.element;
	}



	@Override
	public int darNumeroElementos() {
		// TODO Auto-generated method stub
		int size = 0;
		DoubleNode<T> actual = primero;
		while(actual != null){
			actual = actual.next;
			size++;
		}

		return size;
	}

	@Override
	public T darElementoPosicionActual() {
		// TODO Auto-generated method stub
		return nodoActual.element;
	}

	@Override
	public boolean avanzarSiguientePosicion() {
		// TODO Auto-generated method stub
		boolean avanzarSiguiente = false;
		if(nodoActual.next != null){
			nodoActual = nodoActual.next;
			avanzarSiguiente = true;
		}

		return avanzarSiguiente;

	}

	@Override
	public boolean retrocederPosicionAnterior() {
		// TODO Auto-generated method stub
		boolean retroceder = false;
		if(nodoActual.previous != null){
			nodoActual = nodoActual.previous;
			retroceder = true;
		}


		return retroceder;
	}


	private class DoubleNode <T> {

		private T element;
		private DoubleNode<T> next;
		private DoubleNode<T> previous;

		public DoubleNode (T pElement){
			next = null;
			previous = null;
			element = pElement;
		}

		public T getElement(){
			return element;
		}

		public DoubleNode<T> getNext(){
			return next;
		}

		public DoubleNode<T> getPrevious(){
			return previous;
		}

		public void setNext(DoubleNode<T> pNext){
			next = pNext;
		}

		public void setPrevious(DoubleNode<T> pPrevious){
			previous = pPrevious;
		}

	}

	private class MiIterador<T> implements Iterator<T>{

		DoubleNode<T> actualIt;

		public MiIterador(DoubleNode<T> pPrimero){
			actualIt = pPrimero;
		}

		@Override
		public boolean hasNext() {
			// TODO Auto-generated method stub
			return actualIt != null;
		}
		@Override
		public T next() {
			T element = actualIt.getElement();
			actualIt = actualIt.next;
			return element;
		}

		@Override
		public void remove() {
			// TODO Auto-generated method stub
			
		}

	}

}

